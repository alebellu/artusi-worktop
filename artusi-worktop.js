/* LICENSE START */
/*
 * ssoup
 * https://github.com/alebellu/ssoup
 *
 * Copyright 2012 Alessandro Bellucci
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://alebellu.github.com/licenses/GPL-LICENSE.txt
 * http://alebellu.github.com/licenses/MIT-LICENSE.txt
 */
/* LICENSE END */

/* HEADER START */
var env, requirejs, define;
if (typeof module !== 'undefined' && module.exports) {
    env = "node";
    requirejs = require('requirejs');
    define = requirejs.define;

    requirejs.config({
        baseUrl: __dirname,
        nodeRequire: require
    });

    // require all dependant libraries, so they will be available for the client to request.
    var pjson = require('./package.json');
    for (var library in pjson.dependencies) {
        require(library);
    }
} else {
    env = "browser";
    //requirejs = require;
}

({ define: env === "browser"
        ? define
        : function(A,F) { // nodejs.
            var path = require('path');
            var moduleName = path.basename(__filename, '.js');
            console.log("Loading module " + moduleName);
            module.exports = F.apply(null, A.map(require));
            global.registerUrlContext("/" + moduleName + ".js", {
                "static": __dirname,
                defaultPage: moduleName + ".js"
            });
            global.registerUrlContext("/" + moduleName, {
                "static": __dirname
            });
        }
}).
/* HEADER END */
define(['jquery', 'artusi-kitchen-tools'], function($, tools) {

    var drdf = tools.drdf;
    var drff = tools.drff;

    var worktopType = function(context, options) {
        var worktop = this;

        worktop.context = context;
        worktop.initOptions = options;

        /**
         * Worktop IRI.
         */
        worktop.iri = "http://worktop.artusi.ssoup.org/";

        /**
         * Hash map of nodes
         */
        worktop.nodes = {};

        /**
         * Default pathset
         */
        worktop.defaultPathset = undefined;

        /**
         * Cache used to save results.
         */
        if (typeof SSOUP_CACHE_INIT !== 'undefined') {
            var jsonCache = unescape(SSOUP_CACHE_INIT);
            worktop.cache = JSON.parse(jsonCache);
        } else {
            worktop.cache = {};
        }
    };

    worktopType.prototype.init = function(options) {
        var dr = $.Deferred();

        dr.resolve();

        return dr.promise();
    };

    /**
     * Artusi implementation of SSOUP [worktop.addNode](https://github.com/alebellu/ssoup/blob/master/concepts.md#worktop.addHob)
     *
     * @options:
     * 	@node the node to add to the worktop
     */
    worktopType.prototype.addNode = function(options) {
        var worktop = this;

        worktop.nodes[options.node.getIRI()] = options.node;

        return worktop;
    };

    /**
     * Artusi implementation of SSOUP [worktop.getNode](https://github.com/alebellu/ssoup/blob/master/concepts.md#worktop.getDrawer)
     *
     * @options:
     * 	@nodeIRI the IRI of the drawer
     * @returns the node
     */
    worktopType.prototype.getNode = function(options) {
        var worktop = this;

        if (options.nodeIRI) {
            return worktop.nodes[options.nodeIRI];
        }
    };

    /**
     * Artusi implementation of SSOUP [worktop.getFullPathset](https://github.com/alebellu/ssoup/blob/master/concepts.md#worktop.getFullPathset)
     *
     * @return the full pathset for the worktop
     */
    worktopType.prototype.getFullPathset = function(options) {
        var worktop = this;

        var pathset = [];
        $.each(worktop.nodes, function(key, node) {
            pathset.push([node.getIRI()]);
        });

        return pathset;
    }

    /**
     * Artusi implementation of SSOUP [worktop.setDefaultPathset](https://github.com/alebellu/ssoup/blob/master/concepts.md#worktop.setDefaultDataset)
     *
     * @options:
     *	@pathset the default pathset for the worktop
     */
    worktopType.prototype.setDefaultPathset = function(options) {
        var adapter = this;

        adapter.defaultDataset = options.pathset;
    };

    /**
     * Artusi implementation of SSOUP [worktop.getDefaultPathset](https://github.com/alebellu/ssoup/blob/master/concepts.md#worktop.getDefaultPathset)
     *
     * @return the default pathset for the worktop
     */
    worktopType.prototype.getDefaultPathset = function(options) {
        var worktop = this;

        if (worktop.defaultPathset) {
            return worktop.defaultPathset;
        }

        return worktop.getFullPathset();
    };

    /**
     * @options
     *   @message
     *   @recipients
     *   @recipientNodeAttributes
     *   @modality
     *   @postProcess
     *   @cacheResults if true results for this exact request will be cached and reused for future requests.
     */
    worktopType.prototype.publish = function(options) {
        var worktop = this;
        var dr = $.Deferred();

        if (console) {
            console.log("--> ", options.message["sm:taskType"], options);
        }

        var cacheKey;
        if (options.cacheResults) {
            cacheKey = JSON.stringify(options);
            var cachedResult = worktop.cache[cacheKey];
            if (cachedResult) {
                if (console) {
                    console.log("<-- " + options.message["sm:taskType"] + ": returning cached result: ", cachedResult);
                }
                dr.resolve(cachedResult.retValues, cachedResult.nodes, cachedResult.nodesValuesMap);
                return dr.promise();
            }
        }

        var modality = options.modality;
        if (!modality) {
            modality = "swt:FirstAvailableResponse";
        }

        var opt = {
            message: options.message,
            modality: modality,
            postProcess: options.postProcess
        };
        if (options.recipients) {
            opt.pathSet = options.recipients;
        }
        else if (options.recipientNodeAttributes) {
            opt.pathSet = worktop.filterPathSet(worktop.getFullPathset(), options.recipientNodeAttributes);
        }
        else {
            opt.pathSet = worktop.filterPathSet(worktop.getDefaultPathset(), []);
        }

        var drl = worktop.publishMessage(opt, modality);

        var resolve = function(retValues, nodes, nodesValuesMap) {
            var resultToCache = {
                retValues: retValues,
                nodes: nodes,
                nodesValuesMap: nodesValuesMap
            };

            if (options.cacheResults) {
                worktop.cache[cacheKey] = resultToCache;
            }

            if (console) {
                console.log("<-- " + options.message["sm:taskType"] + ": returning result: ", resultToCache);
            }

            dr.resolve(retValues, nodes, nodesValuesMap);
        };

        drl.done(function(retValues, nodes, nodesValuesMap) {
            if (!options.postProcess) {
                resolve(retValues, nodes, nodesValuesMap);
            }
            else if (options.postProcess == "mergeArrays") {
                // merges results from different nodes in a single array :
                // if the result of a node is an array,each item in the array is added to the result.
                var retArray = [];
                if (retValues) {
                    $.each(retValues, function(index, value) {
                        if ($.isArray(value)) {
                            $.each(value, function(innerIndex, innerValue) {
                                retArray.push(innerValue);
                            });
                        }
                        else {
                            retArray.push(value);
                        }
                    });
                }
                resolve(retArray, nodes, nodesValuesMap);
            }
            else if (options.postProcess == "notEmptyAddOrigin") {
                // returns an array of non-null and non-empty values enriched with origin information
                var retArray = [];
                retArray["@type"] = "sw:resultList";
                if (nodes) {
                    var i = 0;
                    $.each(nodes, function(index, nodeIRI) {
                        var valuesArray = nodesValuesMap[nodeIRI];
                        if (valuesArray) {
                            $.each(valuesArray, function(index, value) {
                                if (value["@type"] == "sw:valueWithOrigin") {
                                    if (!$.isArray(value.origin)) {
                                        value.origin = [nodeIRI, value.origin];
                                    } else {
                                        value.origin.splice(0, 0, nodeIRI);
                                    }
                                    retArray[i] = value;
                                } else {
                                    retArray[i] = {
                                        "@type": "sw:valueWithOrigin",
                                        origin: nodeIRI,
                                        value: value
                                    };
                                }
                                i++;
                            });
                        }
                    });
                }
                resolve(retArray, nodes, nodesValuesMap);
            }
        }).fail(drff(dr));

        return dr.promise();
    };

    worktopType.prototype.publishMessage = function(opt, modality) {
        var worktop = this;

        switch (modality) {
        case "swt:BroadcastNoResponse":
            return worktop.broadcastNoResponse(opt);
        case "swt:BroadcastCollectResponses":
            return worktop.broadcastCollectResponses(opt);
        case "swt:BroadcastNotifyResponses":
            return worktop.broadcastMultipleResponses(opt);
        case "swt:SerialNoResponse":
            return worktop.serialNoResponse(opt);
        case "swt:SerialCollectResponses":
            return worktop.serialCollectResponses(opt);
        case "swt:SerialNotifyResponses":
            return worktop.serialMultipleResponses(opt);
        case "swt:FirstAvailableNoResponse":
            return worktop.firstAvailableNoResponse(opt);
        case "swt:FirstAvailableResponse":
            return worktop.firstAvailableResponse(opt);
        }
    };

    worktopType.prototype.subscribe = function(options) {};

    worktopType.prototype.contains = function(nodeIRI) {
        var worktop = this;

        if (worktop.nodes[nodeIRI]) {
            return true;
        }

        return false;
    };

    worktopType.prototype.deliverMessage = function(node, message) {
        var dr = $.Deferred();

        if (console) {
            console.log("sending ", message.message["sm:taskType"], " to node " + node.getIRI(), message);
        }
        node.onMessage(message).done(function(res) {
            if (res) {
                if (!$.isArray(res) || res["@type"] != "sw:resultList") {
                    res = [res];
                    res["@type"] = "sw:resultList";
                }
            }

            dr.resolve(res);
        }).fail(drff(dr));

        return dr.promise();
    };

    worktopType.prototype.filterPathSet = function(pathSet, nodeAttributesToCheck) {
        var worktop = this;

        var res = [];
        $.each(pathSet, function(index, path) {
            if (path.length >= 1) {
                if (worktop.contains(path[0])) {
                    var ok = true;
                    if (nodeAttributesToCheck) {
                        // check if node has the given attributes and if so res.push
                        var node = worktop.nodes[path[0]];
                        var actualAttributes = node.getAttributes();
                        if (!actualAttributes["all-pass"]) { // an all-pass attribute means that all nodes should be accepted
                            $.each(nodeAttributesToCheck, function(index, nodeAttributeToCheck) {
                                if (!actualAttributes[nodeAttributeToCheck]) {
                                    ok = false;
                                }
                            });
                        }
                    }
                    if (ok) {
                        res.push(path);
                    }
                }
            }
        });

        return res;
    };

    worktopType.prototype.broadcastCollectResponses = function(options) {
        var worktop = this;
        var dr = $.Deferred();

        var drs = [];
        var values = [];
        var nodes = [];
        var nodesValuesMap = {};
        $.each(options.pathSet, function(index, path) {
            var nodeIRI = path[0];
            var node = worktop.nodes[nodeIRI];
            var rest = path.slice(1);

            var opt = {
                message: options.message,
                modality: options.modality,
                postProcess: options.postProcess
            };

            // update recipients only if they were specified already
            if (options.recipients) {
                opt.recipients = rest;
            }

            var drl = worktop.deliverMessage(node, opt).done(function(response) {
                if (response) {
                    if (!$.isArray(response) || response["@type"] != "sw:resultList") {
                        console.error("Response MUST be an array: " + response);
                    } else {
                        values = values.concat(response); // merge response arrays
                        values["@type"] = "sw:resultList";
                        nodes.push(nodeIRI);
                        nodesValuesMap[nodeIRI] = response;
                    }
                }
            });
            drs.push(drl);
        });

        $.when.apply($, drs).done(function() {
            dr.resolve(values, nodes, nodesValuesMap);
        }).fail(drff(dr));

        return dr.promise();
    };

    worktopType.prototype.firstAvailableResponse = function(options) {
        var worktop = this;
        var dr = $.Deferred();

        var sendMessage = function(pathSet) {
            var path = pathSet[0];
            var nodeIRI = path[0];
            var node = worktop.nodes[nodeIRI];
            var rest = path.slice(1);

            var opt = {
                message: options.message,
                modality: options.modality,
                postProcess: options.postProcess
            };

            // update recipients only if they were specified already
            if (options.recipients) {
                opt.recipients = rest;
            }

            var drl = worktop.deliverMessage(node, opt).done(function(responseArray) {
                if (responseArray && responseArray.length > 0) {
                    var nodesValuesMap = {};
                    nodesValuesMap[nodeIRI] = responseArray;
                    var responseValue;
                    for (var i = 0 ; i < responseArray.length ; i ++) {
                        if (responseArray[i]) {
                            responseValue = responseArray[i];
                            break;
                        }
                    }
                    dr.resolve(responseValue, [nodeIRI], nodesValuesMap);
                }
                else {
                    pathSet.splice(0, 1);
                    if (pathSet.length > 0) {
                        sendMessage(pathSet);
                    }
                    else {
                        dr.resolve(undefined);
                    }
                }
            }).fail(drff(dr));
        };

        var pathSet = options.pathSet;
        sendMessage(pathSet);

        return dr.promise();
    };

    return worktopType;
});
